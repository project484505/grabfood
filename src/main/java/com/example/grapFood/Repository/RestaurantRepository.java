package com.example.grapFood.Repository;

import com.example.grapFood.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
    Optional<Restaurant> findById(Long id);
}




//package com.example.grapFood.Repository;
//
//import com.example.grapFood.entity.Restaurant;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.List;
//
//
//public interface RestaurantRepository extends JpaRepository<Restaurant, Long> {
//    List<Restaurant> findAll();
//
//    <T> ScopedValue<T> findById(Long id);
//
//    void saveAll(List<Restaurant> restaurants);
//}
//
