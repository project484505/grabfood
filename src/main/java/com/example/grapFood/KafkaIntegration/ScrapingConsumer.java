package com.example.grapFood.KafkaIntegration;


import com.example.grapFood.services.ScrapingService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ScrapingConsumer {

    private final ScrapingService scrapingService;

    public ScrapingConsumer(ScrapingService scrapingService) {
        this.scrapingService = scrapingService;
    }

    @KafkaListener(topics = "scraping-tasks", groupId = "scraping-group")
    public void listen(String location) {
        scrapingService.scrapeRestaurants(location);
    }
}
