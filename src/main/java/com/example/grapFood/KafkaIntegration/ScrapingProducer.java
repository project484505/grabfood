package com.example.grapFood.KafkaIntegration;

import org.springframework.stereotype.Service;
import org.springframework.kafka.core.KafkaTemplate;


@Service
public class ScrapingProducer {

    private final KafkaTemplate<String, String> kafkaTemplate;

    public ScrapingProducer(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendScrapingTask(String location) {
        kafkaTemplate.send("scraping-tasks", location);
    }
}
