package com.example.grapFood;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrapFoodApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrapFoodApplication.class, args);
	}

}
