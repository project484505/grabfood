package com.example.grapFood.controller;

import com.example.grapFood.Repository.RestaurantRepository;
import com.example.grapFood.entity.Restaurant;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/restaurants")
public class RestaurantController {

    private final RestaurantRepository restaurantRepository;

    public RestaurantController(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }

    @GetMapping
    public List<Restaurant> getAllRestaurants() {
        return restaurantRepository.findAll();
    }

    @GetMapping("/{id}")
    public Restaurant getRestaurantById(@PathVariable Long id) {
        return restaurantRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Restaurant not found"));
    }

    @PostMapping("/scrape")
    public ResponseEntity<Void> scrapeRestaurants(@RequestParam String location) {
        // Trigger scraping
        // scrapingService.scrapeRestaurants(location);
        return ResponseEntity.accepted().build();
    }
}


//package com.example.grapFood.controller;
//
//import com.example.grapFood.Repository.RestaurantRepository;
//import com.example.grapFood.entity.Restaurant;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.server.ResponseStatusException;
//
//import javax.persistence.Id;
//import java.util.List;
//
//@RestController
//@RequestMapping("/api/restaurants")
//public class RestaurantController {
//
//    private final RestaurantRepository restaurantRepository;
//
//    public RestaurantController(RestaurantRepository restaurantRepository) {
//        this.restaurantRepository = restaurantRepository;
//    }
//
//    @GetMapping
//    public List<Restaurant> getAllRestaurants() {
//        return restaurantRepository.findAll();
//    }
//
//    @GetMapping("/{id}")
//    public Restaurant getRestaurantById(@PathVariable Long id) {
//        return restaurantRepository.findById(id)
//                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//    }
//
//    @PostMapping("/scrape")
//    public ResponseEntity<Void> scrapeRestaurants(@RequestParam String location) {
//        // Trigger scraping
//        RestaurantController scrapingService = null;
//        scrapingService.scrapeRestaurants(location);
//        return ResponseEntity.accepted().build();
//    }
//}
