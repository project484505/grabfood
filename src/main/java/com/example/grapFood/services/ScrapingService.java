//package com.example.grapFood.services;
//
//import com.example.grapFood.Repository.RestaurantRepository;
//import com.example.grapFood.entity.Restaurant;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Service;
//import sun.net.ftp.FtpClient;
//
//import javax.lang.model.util.Elements;
//import javax.swing.text.Document;
//import javax.swing.text.Element;
//import java.util.ArrayList;
//import java.util.List;
//
//@Service
//public class ScrapingService {
//
//    private final RestaurantRepository restaurantRepository;
//
//    public ScrapingService(RestaurantRepository restaurantRepository) {
//        this.restaurantRepository = restaurantRepository;
//    }
//
//    @Async
//    public void scrapeRestaurants(String location) {
//        // Scraping logic using Jsoup
//        String url = "https://food.grab.com/sg/en/restaurants?location=" + location;
//        FtpClient Jsoup = null;
//        Document doc = Jsoup.connect(url).get();
//        Elements restaurantElements = doc.getStartPosition(".restaurant-class"); // Update with actual selector
//
//        List<Restaurant> restaurants = new ArrayList<>();
//        for (Element element : restaurantElements) {
//            Restaurant restaurant = new Restaurant();
//            restaurant.setRating(element.getStartOffset());
//            restaurant.setRating(element.getElement(Integer.parseInt(".cuisine")).text());
//            // Populate other fields
//            restaurants.add(restaurant);
//        }
//
//        restaurantRepository.saveAll(restaurants);
//    }
//}
//
package com.example.grapFood.services;

import com.example.grapFood.Repository.RestaurantRepository;
import com.example.grapFood.entity.Restaurant;
//import com.example.grapFood.repository.RestaurantRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ScrapingService {

    private final RestaurantRepository restaurantRepository;

    public ScrapingService(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }

    @Async
    public void scrapeRestaurants(String location) {
        try {
            // Scraping logic using Jsoup
            String url = "https://food.grab.com/sg/en/restaurants?search=" + location;
            Document doc = Jsoup.connect(url).get();
            Elements restaurantElements = doc.select(".restaurant-card"); // Update with actual selector

            List<Restaurant> restaurants = new ArrayList<>();
            for (Element element : restaurantElements) {
                Restaurant restaurant = new Restaurant();
                restaurant.setName(element.select(".restaurant-name").text());
                restaurant.setCuisine(element.select(".restaurant-cuisine").text());
                restaurant.setRating(Double.parseDouble(element.select(".restaurant-rating").text()));
                restaurant.setEstimatedDeliveryTime(Integer.parseInt(element.select(".delivery-time").text().replaceAll("[^0-9]", "")));
                restaurant.setDistance(Double.parseDouble(element.select(".distance").text().replaceAll("[^0-9.]", "")));
                restaurant.setPromotionalOffers(element.select(".promo-offers").text());
                restaurant.setNotice(element.select(".notice").text());
                restaurant.setImageLink(element.select(".restaurant-image").attr("src"));
                restaurant.setIsPromoAvailable(!element.select(".promo-available").isEmpty());
                restaurant.setRestaurantId(element.attr("data-id"));
                restaurant.setLatitude(Double.parseDouble(element.attr("data-latitude")));
                restaurant.setLongitude(Double.parseDouble(element.attr("data-longitude")));
                restaurant.setDeliveryFee(Double.parseDouble(element.select(".delivery-fee").text().replaceAll("[^0-9.]", "")));

                restaurants.add(restaurant);
            }

            restaurantRepository.saveAll(restaurants);
        } catch (IOException e) {
            e.printStackTrace();
            // Handle the exception properly, e.g., log it or send a notification
        }
    }
}
