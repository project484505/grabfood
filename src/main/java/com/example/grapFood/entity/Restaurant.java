package com.example.grapFood.entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String cuisine;
    private Double rating;
    private Integer estimatedDeliveryTime;
    private Double distance;
    private String promotionalOffers;
    private String notice;
    private String imageLink;
    private Boolean isPromoAvailable;
    private String restaurantId;
    private Double latitude;
    private Double longitude;
    private Double deliveryFee;

    // Default constructor
    public Restaurant() {}

    // Getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getEstimatedDeliveryTime() {
        return estimatedDeliveryTime;
    }

    public void setEstimatedDeliveryTime(Integer estimatedDeliveryTime) {
        this.estimatedDeliveryTime = estimatedDeliveryTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getPromotionalOffers() {
        return promotionalOffers;
    }

    public void setPromotionalOffers(String promotionalOffers) {
        this.promotionalOffers = promotionalOffers;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public Boolean getIsPromoAvailable() {
        return isPromoAvailable;
    }

    public void setIsPromoAvailable(Boolean isPromoAvailable) {
        this.isPromoAvailable = isPromoAvailable;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cuisine='" + cuisine + '\'' +
                ", rating=" + rating +
                ", estimatedDeliveryTime=" + estimatedDeliveryTime +
                ", distance=" + distance +
                ", promotionalOffers='" + promotionalOffers + '\'' +
                ", notice='" + notice + '\'' +
                ", imageLink='" + imageLink + '\'' +
                ", isPromoAvailable=" + isPromoAvailable +
                ", restaurantId='" + restaurantId + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", deliveryFee=" + deliveryFee +
                '}';
    }
}


//package com.example.grapFood.entity;
//
//
//import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
//import jakarta.persistence.GenerationType;
//import org.springframework.data.annotation.Id;
//
//@Entity
//public class Restaurant {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//    private String name;
//    private String cuisine;
//    private Double rating;
//    private Integer estimatedDeliveryTime;
//    private Double distance;
//    private String promotionalOffers;
//    private String notice;
//    private String imageLink;
//    private Boolean isPromoAvailable;
//    private String restaurantId;
//    private Double latitude;
//    private Double longitude;
//    private Double deliveryFee;
//
//    public void setDeliveryFee(double v) {
//    }
//
//    public void setLongitude(double v) {
//    }
//
//    public void setLatitude(double v) {
//    }
//
//    public void setRating(double v) {
//    }
//
//    public void setEstimatedDeliveryTime(int i) {
//    }
//
//    public void setDistance(double v) {
//    }
//
//    public void setIsPromoAvailable(boolean b) {
//    }
//
//    // Getters and setters
//}
