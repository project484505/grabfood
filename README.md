# Grab Food Scraper Project

## Introduction
This project scrapes restaurant data from Grab Food and provides it via a REST API. It uses Spring Boot, SQL, Kafka, and OOP concepts.

## Project Structure
- **src/main/java**: Contains the Java source code.
- **src/main/resources**: Configuration files.

## Setup Instructions
1. Clone the repository.
2. Update `application.properties` with your MySQL and Kafka configurations.
3. Build and run the application.

## API Endpoints
- `GET /api/restaurants`: Retrieve all restaurants.
- `GET /api/restaurants/{id}`: Retrieve a specific restaurant.
- `POST /api/restaurants/scrape`: Trigger scraping for a location.

## Scraping Logic
The scraper uses Jsoup to parse HTML and extract restaurant details. It supports multithreading for efficiency.

## Kafka Integration
Kafka is used to queue scraping tasks. The producer sends tasks, and the consumer processes them.

## Database Schema
The restaurant data is stored in a MySQL database.

## Testing
Ensure compliance with terms of service. Test on small datasets.

## Challenges and Solutions
- **Challenge 1**: Dynamic content loading.
  - **Solution**: Used Jsoup with proxy settings.
- **Challenge 2**: Rate limiting.
  - **Solution**: Implemented request throttling.

## Future Improvements
- Improve error handling.
- Enhance data processing.
